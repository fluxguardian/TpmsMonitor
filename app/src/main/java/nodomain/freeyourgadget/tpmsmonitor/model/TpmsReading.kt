package nodomain.freeyourgadget.tpmsmonitor.model

import androidx.room.*

@Entity(foreignKeys = [ForeignKey(
    entity = TpmsSensor::class,
    parentColumns = arrayOf("mac_address"),
    childColumns = arrayOf("mac_address"),
    onDelete = ForeignKey.CASCADE)],
    indices = [Index("mac_address")]
)
data class TpmsReading (
    @PrimaryKey
    var timestamp: Long,
    @ColumnInfo(name = "mac_address") var macAddress: String,
    var pressure: Int,
    var temperature: Int
)