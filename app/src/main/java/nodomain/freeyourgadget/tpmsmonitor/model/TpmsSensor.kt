package nodomain.freeyourgadget.tpmsmonitor.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TpmsSensor (
@PrimaryKey
@ColumnInfo(name = "mac_address") var macAddress: String,
@ColumnInfo(name = "bt_name") var btName: String?
)