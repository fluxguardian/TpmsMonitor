package nodomain.freeyourgadget.tpmsmonitor.services

import android.Manifest
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.content.PermissionChecker
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.ArrayList
import android.bluetooth.le.ScanFilter
import android.os.ParcelUuid
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsReading
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsSensor
import nodomain.freeyourgadget.tpmsmonitor.viewmodel.TpmsViewModel

class BackgroundScanner : Service() {

    private lateinit var viewModel: TpmsViewModel


    private val mScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            parseAdvertising(result!!)
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>) {
            for (result in results) {
                parseAdvertising(result)
            }
        }

        fun parseAdvertising(scanResult: ScanResult) {

            val tpmsSensor = TpmsSensor(scanResult.device.address, scanResult.device.name)
            viewModel.insertTpmsSensor(tpmsSensor)

            val raw = scanResult.scanRecord.getManufacturerSpecificData(0x0100)

            val gen = ByteBuffer.wrap(raw!!)
            gen.order(ByteOrder.LITTLE_ENDIAN)

            val timestamp = System.currentTimeMillis()//don't use scanResult.timestampNanos - see https://stackoverflow.com/questions/32338115/android-ble-convert-scanresult-timestampnanos-to-system-nanotime
            val rawPressure = gen.getInt(6) // / 100000  for bar
            val rawTemperature = gen.getInt(10) // / 100 for Celsius

            val tpmsReading = TpmsReading(timestamp, scanResult.device.address, rawPressure, rawTemperature)
            viewModel.insertTpmsReading(tpmsReading)

            Log.d("BackgroundScanner", "${scanResult.device?.address} - ${scanResult.device?.name} - Temperature: ${rawTemperature} - Pressure: ${rawPressure}")

        }
    }

    private val bluetoothLeScanner: BluetoothLeScanner
        get() {
            val bluetoothManager = applicationContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val bluetoothAdapter = bluetoothManager.adapter
            return bluetoothAdapter.bluetoothLeScanner
        }

    override fun onCreate() {
        super.onCreate()

        viewModel = TpmsViewModel(application)

        Log.d("BackgroundScanner", "STARTED")
        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            bluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback)
        else
            Log.d("BackgroundScanner", "Cannot start service, permission not granted")

    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        bluetoothLeScanner.stopScan(mScanCallback)
    }


    private fun buildScanFilters(): List<ScanFilter> {
        val scanFilters = ArrayList<ScanFilter>()

        scanFilters.add(ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString("0000fbb0-0000-1000-8000-00805F9B34FB")).build())

        return scanFilters
    }

    private fun buildScanSettings(): ScanSettings {
        val builder = ScanSettings.Builder()
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
        return builder.build()
    }
}
