package nodomain.freeyourgadget.tpmsmonitor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import nodomain.freeyourgadget.tpmsmonitor.R
import nodomain.freeyourgadget.tpmsmonitor.databinding.ReadingItemBinding
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsReading
import java.text.SimpleDateFormat

class TpmsReadingAdapter : RecyclerView.Adapter<TpmsReadingAdapter.ViewHolder>() {

    private var readings: List<TpmsReading> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun getItemCount(): Int = readings.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(readings[position])

    class ViewHolder(
        private val parent: ViewGroup,
        private val binding: ReadingItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.reading_item,
            parent,
            false
        )
    ):  RecyclerView.ViewHolder(binding.root) {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS")


        fun bind(item: TpmsReading) {
            val barPressure :Double = (item.pressure / 100000).toDouble()
            val celsiusTemperature :Double = (item.temperature/100).toDouble()

            binding.apply {
                sensorMac = item.macAddress
                timestamp = sdf.format(item.timestamp)
                reading = "Press: $barPressure Temp: $celsiusTemperature"
            }
        }
    }

    internal fun setReadings(tpmsReadings: List<TpmsReading>) {
        this.readings = tpmsReadings
        notifyDataSetChanged()
    }
}