package nodomain.freeyourgadget.tpmsmonitor.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_debug_readings.*
import nodomain.freeyourgadget.tpmsmonitor.R
import nodomain.freeyourgadget.tpmsmonitor.adapter.TpmsReadingAdapter
import nodomain.freeyourgadget.tpmsmonitor.databinding.FragmentDebugReadingsBinding
import nodomain.freeyourgadget.tpmsmonitor.viewmodel.TpmsViewModel

class DebugReadings : Fragment() {

    private lateinit var binding: FragmentDebugReadingsBinding
    private lateinit var viewModel: TpmsViewModel


    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_debug_readings, container, false)

        binding.setLifecycleOwner(this)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this)[TpmsViewModel::class.java]
        val adapter = TpmsReadingAdapter()
        viewModel.tpmsReadingLiveData.observe(this, Observer { readings -> readings?.let {adapter.setReadings(readings)} })
        recyclerview.layoutManager = LinearLayoutManager(context)
        recyclerview.adapter = adapter

        binding.viewModel = viewModel
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
