package nodomain.freeyourgadget.tpmsmonitor.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsReading
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsSensor
import nodomain.freeyourgadget.tpmsmonitor.dao.TpmsReadingDao
import nodomain.freeyourgadget.tpmsmonitor.dao.TpmsSensorDao

@Database(entities = arrayOf(TpmsSensor::class, TpmsReading::class), version = 1)
abstract class AppDatabase:RoomDatabase() {
    abstract fun TpmsSensorDao(): TpmsSensorDao
    abstract fun TpmsReadingDao(): TpmsReadingDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "tpms-database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}